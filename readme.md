This project is a web application that allows a user to enter data required for
generating timetables for a college and then uses that data for generating timetables
on demand.

The web application is developed using Laravel PHP framework and Jquery.

The timetable generation is done using a genetic algorithm that runs as a Laravel
job in the background when timetables are requested.